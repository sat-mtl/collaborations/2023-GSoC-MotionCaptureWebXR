## Motion Capture Module

The Motion Capture module is a Python application designed to harness the power of MediaPipe Pose for accurate human body landmark detection. This module acts as a real-time Web Socket server, transmitting dynamic data to a Web Socket client.

### Installation

To swiftly integrate this project into your workflow, follow these steps:

1. **Clone the Repository** 

2. **Navigate to the Project Directory**: Move into the project directory that you just cloned:
   ```
   cd motion_capture
   ```

3. **Install Dependencies:** Install the required dependencies using the following command:
   ```
   pip install -r requirements.txt
   ```

   - Optional: For a more isolated environment, consider using a virtual environment:
   
     ```bash
     pip install virtualenv
     python3 -m virtualenv venv
     source venv/bin/activate
     ```

### Usage

Once the prerequisites are in place, employing the Motion Capture module is a breeze:

1. **Launch the Application:** Execute the following command to start the module:
   ```
   python3 main.py
   ```

   - The Web Socket server will instantly become active and open for client connections.
   - Upon a client's connection, a notification will be displayed, signifying successful communication.
   - An auxiliary window will pop up, showcasing real-time pose detections.
   - The module will capture the precise landmark coordinates, channeling them through the WebSocket connection to the awaiting client.


### Example of Motion Capture transmission via Web Socket communication protocol

   ![Motion Capture transmission via Web Socket communication protocol](assets/mocap.png)