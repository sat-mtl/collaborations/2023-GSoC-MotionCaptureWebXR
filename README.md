# Motion Capture in WebXR

Welcome to the [Motion Capture in WebXR project](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-caciliefanny-motion-capture-in-webxr/)! This application was initiated during the [Google Summer of Code program](https://summerofcode.withgoogle.com/), with a focus on creating a prototype platform that harnesses motion capture technology to enable users to relive live performances in the realm of Extended Reality (XR).

## Project Overview

During this project, we tackled the challenge of merging motion capture technology with WebXR to create a unique experience where users can immerse themselves in live performances. The project comprised two core modules:

### 1. Motion Capture using MediaPipe Pose

In the [motion capture module](./motion_capture), we implemented motion capture functionality using the open-source real-time pose detection tool, MediaPipe Pose. We integrated this module into a live streaming context using a Web Socket server-side component.

The Web Socket server played a pivotal role by establishing a socket server. It captured real-time video frames from a camera through MediaPipe's pose estimation and transmitted these frames to clients. The transmission was efficiently carried out using a pickled format, accompanied by size information. On the client side, the Web Socket module received these data packets, reconstructed the video frames, and leveraged Python's OpenCV package to present them.

### 2. Augmented Reality (AR) Sessions on the Web using WebXR

The [webxr module](./webxr) objective was to craft Augmented Reality sessions on the web using WebXR technology. To seamlessly integrate different components, we harnessed the power of Web Sockets. This encompassed configuring both server-side and client-side Web Sockets, along with developing a captivating web application.

The web application was constructed using Three.js, a powerful framework for creating 3D content on the web. Within this application, a dynamic 3D scene came to life, featuring essential elements such as a camera, renderer, lighting, and a 3D object. This scene was meticulously designed to be WebXR-compatible, thus providing users with immersive Augmented Reality experiences. An animation loop was thoughtfully implemented to ensure uninterrupted updates and rendering of the 3D object, resulting in a truly engaging interactive encounter. Furthermore, the web application established a WebSocket connection, with received messages being logged to the console for monitoring purposes.

**Note:** Direct manipulation of video frames within the WebXR module for AR sessions is a work-in-progress and presents an exciting area for further development.


## How to Contribute

We believe that open-source collaboration can enhance the potential of this project even further. If you're passionate about XR, motion capture, WebXR, or any related technology, we welcome your contributions. Whether it's bug fixes, feature enhancements, documentation improvements, or innovative ideas, every bit of effort is valued and can make a significant difference.

To contribute:

1. Fork the repository and clone it to your local machine.
2. Choose an existing issue from the issue tracker or propose your own ideas.
3. Create a new branch for your contribution.
4. Make your changes, following the coding guidelines and best practices.
5. Test thoroughly and ensure your code is well-documented.
6. Submit a pull request, and our team will review your contribution.

Let's work together to push the boundaries of Motion Capture in WebXR and create captivating experiences for users around the world. Thank you for considering contributing to this project!

---
*This project is a result of the Google Summer of Code program. You can find more about the original proposal [here](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-caciliefanny-motion-capture-in-webxr/).*