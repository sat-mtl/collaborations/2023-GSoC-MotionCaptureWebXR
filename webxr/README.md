## WebXR Augmented Reality Session

This application currently consists in a proof of concept to demonstrate the capabilities of WebXR, Three.js, and WebGL in creating immersive Augmented Reality (AR) experiences.

### Installation

To get started, follow these installation steps:

1. Clone this repository to your local machine.
2. Open a terminal window and navigate to the project directory.
3. Run the following command to install the required dependencies:

   ```sh
   npm install
   ```

### Usage

After installing the dependencies, you're ready to launch the AR experience. Follow these steps:

1. In the terminal, while still in the project directory, execute the following command:

   ```sh
   npm run start
   ```

2. You'll see an output in the terminal displaying a URL, such as `http://localhost:7201`.
3. Open your web browser and enter the provided URL to access the WebXR AR application.

### WebXR API Usage on Mobile iOS

As of now, the WebXR API is not fully integrated into Safari on iOS devices. Therefore, you'll need to use the WebXR Viewer app as an alternative browser. Follow these steps:

1. Visit the Apple Store and download the **WebXR Viewer** app.
2. Navigate to the [GitLab Page of the project](https://sat-mtl.gitlab.io/collaborations/2023-GSoC-MotionCaptureWebXR/webxr).
3. Tap on the "Start AR" button.
4. Grant permission for camera usage when prompted to experience the AR environment.

### WebXR API Usage on Mobile Android

For Android devices, follow these steps:

1. Open the **Chrome** browser on your Android device.
2. Go to the [GitLab Page of the project](https://sat-mtl.gitlab.io/collaborations/2023-GSoC-MotionCaptureWebXR/webxr).
3. Click on the "Start AR" button.
4. Allow camera access when prompted to enjoy the AR experience.

### WebXR API Usage on Desktop

To experience the AR application on your desktop browser, follow these steps:

1. Open **Chrome** or **Firefox** browser on your computer.
2. Install the **WebXR API Emulator** extension using the links below:
   - [Chrome Extension](https://chrome.google.com/webstore/detail/webxr-api-emulator/mjddjgeghkdijejnciaefnkjmkafnnje/related?hl=en)
   - [Firefox Extension](https://addons.mozilla.org/en-US/firefox/addon/webxr-api-emulator/)
3. Enable the WebXR API Emulator extension.
4. Visit the [GitLab Page of the project](https://sat-mtl.gitlab.io/collaborations/2023-GSoC-MotionCaptureWebXR/webxr).
5. If you encounter a "AR not supported" message:
   - Open the Developer Tools in your browser.
   - Click on "WebXR" in the extended menu.
   - Choose "Samsung Galaxy S8+" (AR) from the dropdown menu.
   - Reload the page.
6. Click the "Start AR" button to begin your augmented reality experience.


### **Example of WebXR API Usage on Desktop**:

   ![Example of WebXR API Usage on Desktop](assets/webxr.png)