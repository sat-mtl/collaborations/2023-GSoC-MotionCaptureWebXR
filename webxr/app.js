const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/html' })
    const readStream = fs.createReadStream('index.html', 'utf-8');
    readStream.pipe(res)
});

server.listen(7201, () => {
    console.log('WebXR server running on http://localhost:7201')
});


const WebSocket = require('ws');
const ws = new WebSocket('ws://localhost:7202')

ws.onmessage = (event) => {
    console.log(event.data);
};

ws.onopen = (event) => {
    ws.send('WebXR module is sending message to MediaPipe Pose module.');
};

// const closeConnection = ws.close();
