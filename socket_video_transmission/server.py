import cv2
import json
import mediapipe as mp
import pickle
import socket
import struct

# Socket Create
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 7202
socket_address = ('localhost', port)

#Socket Bind
server_socket.bind(socket_address)

# Socket Listen
server_socket.listen(5)
print('LISTENNING AT:', socket_address)

# Socket Accept
while True:
    client_socket, addr = server_socket.accept()
    print('GOT CONNECTION FROM:', addr)
    if client_socket:

        msg_from_client = client_socket.recv(1024)
        print(msg_from_client.decode())

        mp_drawing = mp.solutions.drawing_utils
        mp_drawing_styles = mp.solutions.drawing_styles
        mp_pose = mp.solutions.pose

        cap = cv2.VideoCapture(0)

        with mp_pose.Pose(min_detection_confidence=0.7,
                          min_tracking_confidence=0.7) as pose:

            while (cap.isOpened()):
                success, frame = cap.read()
                if not success:
                    print("Ignoring empty camera frame.")
                    continue

                # Close cv2 pop up feed
                if cv2.waitKey(10) & 0xFF == ord('q'):
                    break

                # Convert the BGR image to RGB before processing
                image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                image.flags.writeable = False

                # Make pose detections
                results = pose.process(image)

                # Draw the pose annotation on the image
                image.flags.writeable = True
                image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

                # Render pose annotations
                mp_drawing.draw_landmarks(
                    image,
                    results.pose_landmarks,
                    mp_pose.POSE_CONNECTIONS,
                    landmark_drawing_spec=mp_drawing_styles.
                    get_default_pose_landmarks_style())

                # Visualize the frames and flip the image horizontally
                cv2.imshow('MediaPipe Pose WebSocket Server',
                           cv2.flip(image, 1))

                # Get pose landmarks and all detected points
                landmarks = {}
                try:
                    for mark, data_point in zip(
                            mp_pose.PoseLandmark,
                            results.pose_landmarks.landmark):
                        landmarks[mark.value] = dict(
                            landmark=mark.name,
                            x=data_point.x,
                            y=data_point.y,
                            z=data_point.z,
                            visibility=data_point.visibility)

                    # Transform the dictionary into a json object
                    json_landmarks = json.dumps(landmarks, indent=2)
                except Exception as exc:
                    print(exc)
                    pass

                # client_socket.send(json.dumps(json_landmarks).encode())

                a = pickle.dumps(image)
                message = struct.pack('Q', len(a)) + a
                client_socket.sendall(message)
