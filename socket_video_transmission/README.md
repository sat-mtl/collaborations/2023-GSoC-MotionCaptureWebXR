## Socket Video Transmission Module

This module is used for testing the communication protocols implementation, serving as both a WebSocket server and client. 
It offers seamless real-time transmission of video capture.

### Installation

To get started with the latest version of the project, follow these steps:

1. **Clone the Repository**

2. **Navigate to the Project Directory**: Move into the project directory that you just cloned:
   
   ```
   cd socket_video_transmission
   ```

3. **Install Dependencies**: Install the required dependencies by executing the following command:

   ```
   pip install -r requirements.txt
   ```

   - If you prefer a clean environment, you can set up a virtual environment before installing dependencies:

     ```
     pip install virtualenv
     python3 -m virtualenv venv
     source venv/bin/activate
     ```

### Usage

Once the installation is complete, you can easily run the application using the following steps:

1. **Start the Server**: Run the following command to initiate the WebSocket server:

   ```
   python3 server.py
   ```

   - The WebSocket server will now be up and running, ready to accept client connections.

2. **Start the Client**: In a new terminal window, execute the following command to launch the WebSocket client:

   ```
   python3 client.py
   ```

   - Upon successful connection, a message will be displayed indicating the client's connection to the server.

3. **Visualize Pose Detections**: Two pop-up windows will appear, displaying pose detections from both the server and the client. This visual representation aids in evaluating the effectiveness of the transmission.

**Note**: Make sure that the respective devices running the server and the client are connected to the same network.
