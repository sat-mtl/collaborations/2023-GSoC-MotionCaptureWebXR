import socket, cv2, pickle, struct

# Create socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 7202
client_socket.connect(('localhost', port))
data = b''

# Define letter Q which means an unsigned long integer that will take 8 bytes
payload_size = struct.calcsize('Q')

while True:

    message = 'New client connected'
    client_socket.send(message.encode())

    while len(data) < payload_size:
        packet = client_socket.recv(
            4 * 1024
        )  # Receive the packets and append them to the data, using a 4 kilo of byte buffer
        if not packet:
            break
        data += packet

    # The first 8 bytes contains the size of the packed message
    # So we are using the data from zero to payload_size (8 bytes)
    packed_msg_size = data[:payload_size]
    data = data[payload_size:]

    # The rest of the data contains the video frame
    # So replace data from the payload size onward as the new data
    # Which will only have the frame to display
    msg_size = struct.unpack('Q', packed_msg_size)[0]

    # Get the packed message size
    # Run a while loop until we receive all the data from the client socket for a frame
    while len(data) < msg_size:
        data += client_socket.recv(4 * 1024)
    frame_data = data[:msg_size]
    data = data[msg_size:]

    # Finally the frame data is recovered
    frame = pickle.loads(frame_data)

    cv2.imshow("WebSocket Client", cv2.flip(frame, 1))
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
client_socket.close()
